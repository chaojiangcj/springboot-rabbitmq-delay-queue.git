package org.example.service;

import org.example.config.RabbitConfig;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *@Author: CJ
 *@Date: 2021-9-26 22:14
 */
@Service
public class MsgProductionService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送延时信息
     *
     * @param content 内容
     * @param routingKey routingKey
     * @param delay 延时时间，秒
     */
    public void sendTimeoutMsg(String content, String routingKey, int delay) {
        // 通过广播模式发布延时消息，会广播至每个绑定此交换机的队列，这里的路由键没有实质性作用
        rabbitTemplate.convertAndSend(RabbitConfig.DELAY_EXCHANGE_NAME, routingKey, content, message -> {
            message.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
            // 毫秒为单位，指定此消息的延时时长
            message.getMessageProperties().setDelay(delay * 1000);
            return message;
        });
    }

    /**
     * 发送普通消息
     *
     * @param routingKey 路由key
     * @param content 内容
     */
    public void sendMsg(String routingKey, String content) {
        // DirectExchange类型的交换机，必须指定对应的路由键
        rabbitTemplate.convertAndSend(RabbitConfig.ORDER_PAY_Exchange_Name, routingKey, content);
    }
}
