package org.example;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@Slf4j
public class ProducerApplicaton {

    public static void main(String[] args) {
        SpringApplication.run(ProducerApplicaton.class, args);
        log.info("------rabbitmq producer running------");
    }
}
