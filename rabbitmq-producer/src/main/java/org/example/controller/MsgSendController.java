package org.example.controller;

import org.example.config.RabbitConfig;
import org.example.service.MsgProductionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *@Author: CJ
 *@Date: 2021-9-26 23:52
 */
@Controller
public class MsgSendController {

    @Autowired
    private MsgProductionService msgProductionService;

    @GetMapping(path = "/sendMsg")
    @ResponseBody
    public String sendMsg() {
        // 发送多个延时消息
        msgProductionService.sendTimeoutMsg("hello1", "routingKey1", 40);
        msgProductionService.sendTimeoutMsg("hello2", "routingKey2", 20);
        msgProductionService.sendTimeoutMsg("hello3", "routingKey3", 60);

        // 发送普通消息
        msgProductionService.sendMsg(RabbitConfig.ORDER_PAY_ROUTING_KEY, "weixin");
        msgProductionService.sendMsg(RabbitConfig.ORDER_PAY_ROUTING_KEY, "alipay");
        return "success";
    }
}
